FROM python:3-buster

RUN apt update && \
	apt install -y -q python3-venv ffmpeg libjpeg-dev zlibc zlib1g zlib1g-dev

RUN git clone --recurse-submodules https://github.com/azlux/botamusique.git /srv/botamusique
WORKDIR /srv/botamusique
RUN python3 -m venv venv
RUN venv/bin/pip install wheel
RUN venv/bin/pip install -r requirements.txt

ENTRYPOINT ["venv/bin/python", "mumbleBot.py"]
